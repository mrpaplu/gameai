﻿using System;
using GameAI.Components.Fuzzy;
using GameAI.Components.Fuzzy.Terms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameAITest
{
    [TestClass]
    public class FuzzyTest
    {
        private FuzzyModule Fuzzy { get; set; }

        [TestMethod]
        public void FuzzyTest1()
        {
            Fuzzy = new FuzzyModule();

            FuzzyVariable distanceToTarget = Fuzzy.CreateFLV("DistanceToTarget");

            Set targetClose = distanceToTarget.AddLeftShoulder("TargetClose", 0, 25, 100);
            Set targetMedium = distanceToTarget.AddTriangular("TargetMedium", 25, 150, 300);
            Set targetFar = distanceToTarget.AddRightShoulder("TargetFar", 150, 300, 400);

            FuzzyVariable ammo = Fuzzy.CreateFLV("Ammo");

            Set ammoLow = ammo.AddLeftShoulder("AmmoLow", 0, 0, 10);
            Set ammoMedium = ammo.AddTriangular("AmmoMedium", 0, 10, 30);
            Set ammoHigh = ammo.AddRightShoulder("AmmoHigh", 10, 30, 40);

            FuzzyVariable desirable = Fuzzy.CreateFLV("Desirability");

            Set unDesirable = desirable.AddLeftShoulder("UnDesirable", 0, 25, 50);
            Set desirableSet = desirable.AddTriangular("Desirable", 25, 50, 75);
            Set veryDesirable = desirable.AddRightShoulder("VeryDesirable", 50, 75, 100);

            Fuzzy.AddRule(new And(targetClose, ammoHigh), unDesirable);
            Fuzzy.AddRule(new And(targetClose, ammoMedium), unDesirable);
            Fuzzy.AddRule(new And(targetClose, ammoLow), unDesirable);

            Fuzzy.AddRule(new And(targetMedium, ammoHigh), veryDesirable);
            Fuzzy.AddRule(new And(targetMedium, ammoMedium), veryDesirable);
            Fuzzy.AddRule(new And(targetMedium, ammoLow), desirableSet);

            Fuzzy.AddRule(new And(targetFar, ammoHigh), desirableSet);
            Fuzzy.AddRule(new And(targetFar, ammoMedium), desirableSet);
            Fuzzy.AddRule(new And(targetFar, ammoLow), unDesirable);

            Fuzzy.Fuzzify("DistanceToTarget", 200);
            Fuzzy.Fuzzify("Ammo", 8);

            double expected = 60.4166666666667;
            double actual = Fuzzy.Defuzzify("Desirability");

            Assert.IsTrue(Math.Abs(expected - actual) < 0.00005);
        }
    }
}
