﻿using System;
using ExtensionMethods;
using GameAI;
using GameAI.Components.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace GameAITest
{
    [TestClass]
    public class EntityTest
    {
        [TestMethod]
        public void Entity_CoversLocation()
        {
            Game1 game = new Game1();
            Vector2 position = new Vector2(100, 100);
            Entity e = new Wall(game, position)
            {
                Size = new Vector2(25, 25)
            };
            Vector2 location = new Vector2(101, 101);

            const bool expected = true;
            bool actual = e.CoversLocation(location);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Entity_DoesNotCoverLocation()
        {
            Game1 game = new Game1();
            Vector2 position = new Vector2(100, 100);
            Entity e = new Wall(game, position)
            {
                Size = new Vector2(25, 25)
            };
            Vector2 location = new Vector2(126, 126);

            const bool expected = false;
            bool actual = e.CoversLocation(location);

            Assert.AreEqual(expected, actual);
        }
    }
}
