using System;
using GameAI.Components;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities;
using GameAI.Components.Gui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameAI
{
    /// <summary>
    /// This is the main type for your Game
    /// </summary>
    public class Game1 : Game
    {
        public GraphicsDeviceManager Graphics { get; set; }
        public SpriteBatch SpriteBatch { get; set; }
        public World World { get; set; }
        public bool DebuggingGraph { get; set; }
        public bool DebuggingGoals { get; set; }

        public Game1()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the Game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            World = new World(this);
            Components.Add(World);
            Components.Add(new Statistics(this, World.Player));
            Components.Add(new Cursor(this));
            Services.AddService(typeof(World), World);
            Services.AddService(typeof(Player), World.Player);
            Services.AddService(typeof(Graph), World.Graph);
            Services.AddService(typeof(Base), World.Base);
            
            DebuggingGraph = false;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per Game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per Game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the Game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the Game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            KeyboardState keyState = Keyboard.GetState();
            DebuggingGraph = keyState.IsKeyDown(Keys.D);
            DebuggingGoals = keyState.IsKeyDown(Keys.G);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the Game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }
    }
}
