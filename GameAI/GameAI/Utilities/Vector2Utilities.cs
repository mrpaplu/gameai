﻿using System;
using Microsoft.Xna.Framework;

namespace ExtensionMethods
{
    public static class Vector2Utilities
    {
        public static Vector2 Truncate(this Vector2 original, float max)
        {
            if (original == Vector2.Zero)
                return original;
            if (original.Length() > max)
            {
                original.Normalize();

                original *= max;
            }
            return original;
        }

        public static Vector2 Perpendicular(Vector2 original)
        {
            //To create a perpendicular vector switch X and Y, then make Y negative
            float x = original.X;
            float y = original.Y;

            y = -y;

            return new Vector2(y, x);
        }

        public static float Angle(Vector2 original)
        {
            return (float) Math.Atan2(original.X, -original.Y);
        }
    }
}
