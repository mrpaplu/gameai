﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ExtensionMethods
{
    public static class SpriteBatchUtilities
    {
        /// <summary>
        /// http://gamedev.stackexchange.com/questions/26013/drawing-a-texture-line-between-two-vectors-in-xna-wp7
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="texture"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="width"></param>
        /// <param name="color"></param>
        public static void DrawLine(this SpriteBatch spriteBatch, Texture2D texture, Vector2 begin, Vector2 end, int width, Color color)
        {
            Rectangle r = new Rectangle((int)begin.X, (int)begin.Y, (int)(end - begin).Length() + width, width);
            Vector2 v = Vector2.Normalize(begin - end);
            float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
            if (begin.Y > end.Y) angle = MathHelper.TwoPi - angle;
            spriteBatch.Draw(texture, r, null, color, angle, Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}
