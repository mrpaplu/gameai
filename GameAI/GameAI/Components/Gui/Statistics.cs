using System;
using GameAI.Components.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAI.Components.Gui
{
    /// <summary>
    /// This is a Game component that implements IUpdateable.
    /// </summary>
    public class Statistics : DrawableGameComponent
    {
        Player Player { get; set; }
        new Game1 Game { get; set; }
        private SpriteFont _spriteFont;
        private Texture2D _guiTexture;

        public Statistics(Game1 game, Player player)
            : base(game)
        {
            Game = game;
            Player = player;

            DrawOrder = 10;
        }

        public override void Initialize()
        {
            _spriteFont = Game.Content.Load<SpriteFont>("StatisticsFont");
            _guiTexture = Game.Content.Load<Texture2D>("gui");
            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            Game.SpriteBatch.Begin();

            Vector2 position = new Vector2(Game.Graphics.PreferredBackBufferWidth / 2, Game.Graphics.PreferredBackBufferHeight + 25);
            Game.SpriteBatch.Draw(
                _guiTexture,
                position,
                null,
                Color.White,
                0,
                new Vector2((float)(_guiTexture.Width / 2) - 1, (float)(_guiTexture.Height / 2) - 1),
                0.5f,
                SpriteEffects.None,
                1
            );

            Game.SpriteBatch.DrawString(
                _spriteFont,
                "Crystals: " + Math.Round(Game.World.Player.Crystals),
                new Vector2(position.X, position.Y - 50), 
                Color.Blue,
                0,
                new Vector2(80, 25),
                1,
                SpriteEffects.None,
                1
                );
            Game.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
