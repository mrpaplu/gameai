using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameAI.Components.Gui
{
    /// <summary>
    /// This is a Game component that implements IUpdateable.
    /// </summary>
    public class Cursor : DrawableGameComponent
    {
        private Texture2D _texture;
        private Vector2 _position;
        private readonly Game1 _game;

        public Cursor(Game1 game)
            : base(game)
        {
            _game = game;
            DrawOrder = 11;
        }

        /// <summary>
        /// Allows the Game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            _texture = _game.Content.Load<Texture2D>("cursor");

            base.Initialize();
        }

        /// <summary>
        /// Allows the Game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            _position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Begin();
            _game.SpriteBatch.Draw(_texture, _position, Color.White);
            _game.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
