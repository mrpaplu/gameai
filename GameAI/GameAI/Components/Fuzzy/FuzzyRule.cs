﻿namespace GameAI.Components.Fuzzy
{
    public class FuzzyRule
    {
        #region Properties and fields

        /// <summary>
        /// Antecedent is usually a composite of several fuzzy sets and operators
        /// </summary>
        private FuzzyTerm Antecedent { get; set; }

        /// <summary>
        /// Consequence is usually a single fuzzy set, but can be several ANDed together
        /// </summary>
        private FuzzyTerm Consequence { get; set; }

        #endregion

        #region Constructor

        public FuzzyRule(FuzzyTerm antecedent, FuzzyTerm consequence)
        {
            Antecedent = antecedent;
            Consequence = consequence;
        }

        public void SetConfidenceOfConsequentToZero()
        {
            Consequence.ClearDOM();
        }

        public void Calculate()
        {
            Consequence.ORwithDOM(Antecedent.GetDOM());
        }

        #endregion
    }
}
