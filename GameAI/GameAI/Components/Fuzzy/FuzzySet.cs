﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Components.Fuzzy
{
    public abstract class FuzzySet
    {
        #region Properties and fields

        /// <summary>
        /// This is the maximum of the set's membership function.
        /// </summary>
        public double RepresentativeValue { get; protected set; }

        /// <summary>
        /// This holds the degree of membership in this set
        /// </summary>
        public double Dom { get; set; }

        #endregion

        #region Constructor

        protected FuzzySet(double repVal)
        {
            RepresentativeValue = repVal;
            Dom = 0.0;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Return the degree of membership in this set of the given value.
        /// </summary>
        /// <returns></returns>
        public abstract double CalculateDom(double val);

        /// <summary>
        /// If this fuzzy set is part of a consequent FLV and it is fired by a rule
        /// then this method sets the DOM to the maximum of the parameter value or 
        /// the set's existing dom value
        /// </summary>
        /// <param name="val"></param>
        public void ORwithDOM(double val)
        {
            if (val > Dom)
                Dom = val;
        }

        public void ClearDOM()
        {
            Dom = 0.0;
        }

        #endregion
    }
}
