﻿namespace GameAI.Components.Fuzzy.Sets
{
    public class LeftShoulder : FuzzySet
    {
        #region Fields and properties

        /// <summary>
        /// The peak of this FLV
        /// </summary>
        private double _peak;

        /// <summary>
        /// The left offset of this FLV
        /// </summary>
        private double _left;

        /// <summary>
        /// The right offset of this FLV
        /// </summary>
        private double _right;

        #endregion

        #region Constructor

        public LeftShoulder(double left, double peak, double right)
            : base(((peak - left) + peak) / 2)
        {
            _peak = peak;
            _left = left;
            _right = right;
        }

        #endregion

        #region Set

        /// <summary>
        /// Calculates the degree of membership for a certain value
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public override double CalculateDom(double val)
        {
            // Don't divide by zero
            if (_left == 0.0 && val == _peak)
                return 1.0;

            // Find DOM left of peak
            if (val < _peak)
            {
                return 1.0;
            }

            // Find DOM if right of center
            if (val >= _peak && val < (_peak + _right))
            {
                double grad = 1.0 / _right;
                return grad * (val - _peak) + 1.0;
            }

            // Out of range, return zero
            return 0.0;
        }

        #endregion
    }
}
