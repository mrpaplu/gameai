﻿namespace GameAI.Components.Fuzzy
{
    public abstract class FuzzyTerm
    {
        /// <summary>
        /// Retrieves the degree of membership of the term
        /// </summary>
        /// <returns></returns>
        public abstract double GetDOM();

        /// <summary>
        /// Clears the degree of membership of the term
        /// </summary>
        public abstract void ClearDOM();

        /// <summary>
        /// Method for updating the DOM of a consquent when a rule fires
        /// </summary>
        /// <param name="value"></param>
        public abstract void ORwithDOM(double value);
    }
}
