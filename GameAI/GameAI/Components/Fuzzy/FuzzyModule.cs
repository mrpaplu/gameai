﻿using System;
using System.Collections.Generic;

namespace GameAI.Components.Fuzzy
{
    public class FuzzyModule
    {
        #region Properties and fields

        /// <summary>
        /// A map of all fuzzy variables
        /// </summary>
        Dictionary<string, FuzzyVariable> Variables { get; set; }

        /// <summary>
        /// All fuzzy rules
        /// </summary>
        List<FuzzyRule> Rules { get; set; } 

        #endregion

        #region Constructor

        public FuzzyModule()
        {
            Variables = new Dictionary<string, FuzzyVariable>();
            Rules = new List<FuzzyRule>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates an "empty" fuzzy variable
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public FuzzyVariable CreateFLV(string name)
        {
            Variables.Add(name, new FuzzyVariable());
            return Variables[name];
        }

        /// <summary>
        /// Adds a rule to the rule list
        /// </summary>
        /// <param name="antecedent"></param>
        /// <param name="consequence"></param>
        public void AddRule(FuzzyTerm antecedent, FuzzyTerm consequence)
        {
            FuzzyRule rule = new FuzzyRule(antecedent, consequence);
            Rules.Add(rule);
        }

        /// <summary>
        /// This method calls the Fuzzify method of the variable with the same name
        //  as the key
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void Fuzzify(string name, double value)
        {
            if (Variables.ContainsKey(name))
            {
                Variables[name].Fuzzify(value);
            }
        }

        /// <summary>
        /// Given a fuzzy variable and a deffuzification method this returns a 
        //  crisp value
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public double Defuzzify(string name)
        {
            if (!Variables.ContainsKey(name))
                return 0;

            SetConfidencesOfConsequentsToZero();

            foreach (FuzzyRule rule in Rules)
            {
                rule.Calculate();
            }

            return Variables[name].DefuzzifyMaxAV();
        }

        /// <summary>
        /// Zeros the DOMs of the consequents of each rule
        /// </summary>
        private void SetConfidencesOfConsequentsToZero()
        {
            foreach (FuzzyRule rule in Rules)
            {
                rule.SetConfidenceOfConsequentToZero();
            }
        }

        #endregion
    }
}
