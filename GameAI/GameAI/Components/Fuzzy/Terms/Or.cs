﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Components.Fuzzy.Terms
{
    public class Or : FuzzyTerm
    {
        #region Fields and properties

        /// <summary>
        /// List of terms of and
        /// </summary>
        private List<FuzzyTerm> Terms { get; set; } 

        #endregion

        #region Constructor

        public Or(FuzzyTerm term1, FuzzyTerm term2)
        {
            Terms = new List<FuzzyTerm>();
            Terms.Add(term1);
            Terms.Add(term2);
        }

        public Or(FuzzyTerm term1, FuzzyTerm term2, FuzzyTerm term3)
        {
            Terms = new List<FuzzyTerm>();
            Terms.Add(term1);
            Terms.Add(term2);
            Terms.Add(term3);
        }

        public Or(FuzzyTerm term1, FuzzyTerm term2, FuzzyTerm term3, FuzzyTerm term4)
        {
            Terms = new List<FuzzyTerm>();
            Terms.Add(term1);
            Terms.Add(term2);
            Terms.Add(term3);
            Terms.Add(term4);
        }

        #endregion

        #region FuzzyTerm

        public override double GetDOM()
        {
            double maxDom = double.MinValue;

            foreach (FuzzyTerm t in Terms)
            {
                if (t.GetDOM() > maxDom)
                    maxDom = t.GetDOM();
            }

            return maxDom;
        }

        public override void ClearDOM()
        {
            foreach(FuzzyTerm t in Terms)
                t.ClearDOM();
        }

        public override void ORwithDOM(double value)
        {
            foreach (FuzzyTerm t in Terms)
                t.ORwithDOM(value);
        }

        #endregion
    }
}
