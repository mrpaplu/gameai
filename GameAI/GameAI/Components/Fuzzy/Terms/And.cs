﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Components.Fuzzy.Terms
{
    public class And : FuzzyTerm
    {
        #region Fields and properties

        /// <summary>
        /// List of terms of and
        /// </summary>
        private List<FuzzyTerm> Terms { get; set; } 

        #endregion

        #region Constructor

        public And(FuzzyTerm term1, FuzzyTerm term2)
        {
            Terms = new List<FuzzyTerm>();
            Terms.Add(term1);
            Terms.Add(term2);
        }

        public And(FuzzyTerm term1, FuzzyTerm term2, FuzzyTerm term3)
        {
            Terms = new List<FuzzyTerm>();
            Terms.Add(term1);
            Terms.Add(term2);
            Terms.Add(term3);
        }

        public And(FuzzyTerm term1, FuzzyTerm term2, FuzzyTerm term3, FuzzyTerm term4)
        {
            Terms = new List<FuzzyTerm>();
            Terms.Add(term1);
            Terms.Add(term2);
            Terms.Add(term3);
            Terms.Add(term4);
        }

        #endregion

        #region FuzzyTerm

        public override double GetDOM()
        {
            double minDom = double.MaxValue;

            foreach (FuzzyTerm t in Terms)
            {
                if (t.GetDOM() < minDom)
                    minDom = t.GetDOM();
            }

            return minDom;
        }

        public override void ClearDOM()
        {
            foreach(FuzzyTerm t in Terms)
                t.ClearDOM();
        }

        public override void ORwithDOM(double value)
        {
            foreach (FuzzyTerm t in Terms)
                t.ORwithDOM(value);
        }

        #endregion
    }
}
