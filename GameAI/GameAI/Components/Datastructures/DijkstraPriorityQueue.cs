﻿using System.Collections.Generic;
using System.Linq;

namespace GameAI.Components.Datastructures
{
    public class DijkstraPriorityQueue
    {
        readonly Dictionary<Vertex, float> _distances = new Dictionary<Vertex, float>();

        public void Add(Vertex vertex, float distance)
        {
            _distances.Add(vertex, distance);
        }

        public void Remove(Vertex vertex)
        {
            _distances.Remove(vertex);
        }

        public Vertex GetShortestDistance()
        {
            KeyValuePair<Vertex, float> pair = _distances.FirstOrDefault(x => x.Value == _distances.Min(y => y.Value));
            Remove(pair.Key);
            return pair.Key;
        }

        public bool Any()
        {
            return _distances.Any();
        }

        public void UpdateDistance(Vertex vertex, float distance)
        {
            if(_distances.ContainsKey(vertex))
                _distances[vertex] = distance;
            else
                _distances.Add(vertex, distance);
        }
    }
}
