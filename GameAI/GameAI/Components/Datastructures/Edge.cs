﻿namespace GameAI.Components.Datastructures
{
    public class Edge
    {
        public Vertex Source { get; set; }
        public Vertex Destination { get; set; }
        public float Cost { get; set; }
        public bool Considered { get; set; }

        public Edge(Vertex source, Vertex destination, float cost)
        {
            Source = source;
            Destination = destination;
            Cost = cost;
        }

        public override string ToString()
        {
            return "To " + Destination;
        }
    }
}
