﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;
using GameAI.Components.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameAI.Components.Datastructures
{
    public class Graph : DrawableGameComponent
    {
        #region Fields and properties

        private readonly Game1 _game;
        private Texture2D _dijkstraLine;
        private Texture2D _dijkstraDot;
        private readonly World _world;
        private bool _useAStar;
        private KeyboardState _previousKeyboardState;

        public int Width { get; set; }
        public int Height { get; set; }
        public Dictionary<Vector2, Vertex> Vertices { get; private set; }
        public List<Edge> Edges { get; set; } 
        public int Offset { get; private set; }
        public Stack<Edge> CurrentPath { get; set; }

        #endregion

        #region Constructor

        public Graph(Game1 game, World world, int offset)
            : base(game)
        {
            Offset = offset;
            _game = game;
            _world = world;
            _useAStar = true;

            Width = _game.Graphics.PreferredBackBufferWidth;
            Height = _game.Graphics.PreferredBackBufferHeight;

            Vertices = new Dictionary<Vector2, Vertex>();
            Edges = new List<Edge>();
            CurrentPath = new Stack<Edge>();
        }

        #endregion

        #region DrawableGameComponent

        /// <summary>
        /// Creates all the nodes (vertices) and edges
        /// </summary>
        public override void Initialize()
        {
            _dijkstraDot = _game.Content.Load<Texture2D>("dijkstradot");
            _dijkstraLine = _game.Content.Load<Texture2D>("dijkstraline");

            for (int x = 0; x <= Width; x += Offset)
            {
                for (int y = 0; y <= Height; y += Offset)
                {
                    if (!LocationBlocked(new Vector2(x, y)))
                        CreateVertex(new Vector2(x, y));
                }
            }

            List<Vector2> directions = Directions.GetDirections(Offset);
            for (int x = 0; x <= Width; x += Offset)
            {
                for (int y = 0; y <= Height; y += Offset)
                {
                    Vector2 currentLocation = new Vector2(x, y);
                    foreach (Vector2 direction in directions)
                    {
                        Vector2 neighbour = currentLocation + direction;
                        if (!LocationBlocked(currentLocation) && !LocationBlocked(neighbour))
                        {
                            float cost = 10;
                            if (!Directions.IsOnAxis(direction))
                                cost = (float)Math.Sqrt(cost * cost + cost * cost);
                            AddEdge(currentLocation, neighbour, cost);
                        }
                    }
                }
            }

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            if (!keyboardState.Equals(_previousKeyboardState))
            {
                if (keyboardState.IsKeyDown(Keys.A))
                    _useAStar = !_useAStar;
            }

            _previousKeyboardState = keyboardState;

            base.Update(gameTime);
        }

        /// <summary>
        /// Draws the nodes and vertices
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (_game.DebuggingGraph)
            {
                _game.SpriteBatch.Begin();
                foreach (KeyValuePair<Vector2, Vertex> v in Vertices)
                {
                    _game.SpriteBatch.Draw(
                        _dijkstraDot,
                        v.Value.Location,
                        null,
                        Color.White,
                        0,
                        new Vector2((float)(_dijkstraDot.Width / 2) - 1, (float)(_dijkstraDot.Height / 2) - 1),
                        0.5f,
                        SpriteEffects.None,
                        1);
                }
                foreach (Edge n in Edges)
                {
                    Color color = n.Considered ? Color.Yellow : Color.White;
                    _game.SpriteBatch.DrawLine(
                        _dijkstraLine,
                        n.Source.Location,
                        n.Destination.Location,
                        1,
                        color);
                }
                _game.SpriteBatch.End();
            }

            base.Draw(gameTime);
        }

        #endregion

        #region Graph

        public bool LocationBlocked(Vector2 location)
        {
            return _world.Entities.Exists(e =>
                e.CoversLocation(new Vector2(location.X, location.Y))
                && !(e is MovingEntity) && !(e is Mine) && !(e is Base));
        }

        public void AddEdge(Vector2 source, Vector2 destination, float cost)
        {
            Vertex v = CreateVertex(source);
            Vertex w = CreateVertex(destination);

            Edge e = new Edge(w, v, cost);
            Edges.Add(e);

            v.AddEdge(e);
            w.AddEdge(e);
        }

        public Vertex CreateVertex(Vector2 location)
        {
            if (Vertices.ContainsKey(location))
            {
                return Vertices[location];
            }
            Vertex v = new Vertex(location);
            Vertices.Add(location, v);
            return v;
        }

        public Vertex GetNearestVertex(Vector2 mousePosition)
        {
            if (Vertices.Count <= 0)
                return null;
            return GetNearestVectorRecursively(mousePosition, Offset / 4);
        }

        private Vertex GetNearestVectorRecursively(Vector2 mousePosition, int offset)
        {
            if (offset <= 0)
                offset = 25;
            KeyValuePair<Vector2, Vertex> vertex =
                Vertices.FirstOrDefault(x =>
                    x.Key.X > mousePosition.X - offset
                    && x.Key.X < mousePosition.X + offset
                    && x.Key.Y > mousePosition.Y - offset
                    && x.Key.Y < mousePosition.Y + offset);
            return vertex.Value ?? GetNearestVectorRecursively(mousePosition, offset * 2);
        }

        #endregion

        #region Pathfinding

        public Stack<Edge> GetShortestPath(Vertex source, Vertex destination)
        {
            foreach (Edge ed in Edges)
                ed.Considered = false;
            DijkstraPriorityQueue queue = new DijkstraPriorityQueue();
            Vertex current = null;

            // Initialize all dijkstravertices, set all to visited false and previous null
            // Except for source
            foreach (KeyValuePair<Vector2, Vertex> pair in Vertices)
            {
                Vertex dv = pair.Value;
                dv.Reset();
                if (source == pair.Value)
                {
                    current = dv;
                    current.Distance = 0;
                }
                queue.Add(dv, dv.Distance);
            }

            if (current == null)
                return null;

            int count = 0;
            // Calculate every nodes distance 'til destination is found
            while (queue.Any())
            {
                count++;
                // Get next closest or break if null
                current = queue.GetShortestDistance();
                if (current == null)
                    break;

                foreach (Edge e in current.Adjacent)
                {
                    e.Considered = true;
                    // Use a star or not
                    float distance = 0;
                    if(_useAStar)
                        distance = current.Distance + e.Cost + e.Destination.EuclideanDistanceTo(destination);
                    else
                        distance = current.Distance + e.Cost;

                    if (distance < e.Destination.Distance)
                    {
                        e.Destination.Distance = current.Distance + e.Cost;
                        e.Destination.Previous = current;
                        queue.UpdateDistance(e.Destination, distance);
                    }
                }
                queue.Remove(current);

                // Return if found
                if (current.Location.Equals((destination.Location)))
                {
                    Stack<Edge> path = new Stack<Edge>();
                    while (current.Previous != null)
                    {
                        path.Push(
                        current.Previous.Adjacent.FirstOrDefault(
                        x =>
                            x.Destination ==
                            current));
                        current = current.Previous;
                    }
                    CurrentPath = path;
                    return path;
                }
            }

            return new Stack<Edge>();
        }

        #endregion
    }
}
