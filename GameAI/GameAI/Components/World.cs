using System;
using System.Collections.Generic;
using ExtensionMethods;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAI.Components
{
    /// <summary>
    /// This is a Game component that implements IUpdateable.
    /// </summary>
    public class World : DrawableGameComponent
    {
        #region Properties and fields

        /// <summary>
        /// All entities in the Game world
        /// </summary>
        public List<Entity> Entities { get; set; }

        /// <summary>
        /// The player entity
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// The base station of the player
        /// </summary>
        public Base Base { get; set; }

        /// <summary>
        /// The graph used for pathfinding
        /// </summary>
        public Graph Graph { get; private set; }

        /// <summary>
        /// The Game
        /// </summary>
        public new Game1 Game { get; set; }

        #endregion 

        #region Constructor

        public World(Game1 game)
            : base(game)
        {
            Game = game;
            Entities = new List<Entity>();
            Graph = new Graph(Game, this, 25);

            Base = new Base(Game, new Vector2(400, 200));
            Entities.Add(Base);

            Player = new Player(Game, new Vector2(100, 100), new Vector2(0, 0));
            Entities.Add(Player);
        }

        #endregion

        #region DrawableGameComponent

        /// <summary>
        /// Allows the Game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            InitializeEntities();
            Game.Components.Add(Graph);
        }

        /// <summary>
        /// Allows the Game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            foreach (Entity e in Entities)
                e.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (Entity e in Entities)
                e.Draw(gameTime);

            base.Draw(gameTime);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Tags the obstacles in range of the provided entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="boxLength"></param>
        public void TagObstaclesInRangeOf(Entity entity, float boxLength)
        {
            foreach(Entity e in Entities)
                e.Tagged = Vector2.Distance(entity.Position, e.Position) < boxLength;
        }

        /// <summary>
        /// Initialize the entities in the Game world
        /// </summary>
        private void InitializeEntities()
        {
            Enemy enemy = new Enemy(Game, new Vector2(200, 200), new Vector2(0, 0));
            Entities.Add(enemy);

            Miner henk = new Miner(Game, new Vector2(225, 225), new Vector2(0, 0));
            Entities.Add(henk);

            Entities.Add(new Mine(Game, new Vector2(100, 100)));
            Entities.Add(new Mine(Game, new Vector2(375, 350)));
            Entities.Add(new Mine(Game, new Vector2(125, 400)));
            Entities.Add(new Mine(Game, new Vector2(500, 325)));

            int x = 0;
            int y = 0;

            List<Vector2> wallLocations = new List<Vector2>();

            while (x < Game.Graphics.PreferredBackBufferWidth)
            {
                wallLocations.Add(new Vector2(x, 0));
                x += 25;
            }
            while (y < Game.Graphics.PreferredBackBufferHeight - 25)
            {
                wallLocations.Add(new Vector2(x, y));
                y += 25;
            }
            while (x > 0)
            {
                wallLocations.Add(new Vector2(x, y));
                x -= 25;
            }
            while (y > 0)
            {
                wallLocations.Add(new Vector2(x, y));
                y -= 25;
            }

            wallLocations.Add(new Vector2(300, 300));
            wallLocations.Add(new Vector2(325, 300));
            wallLocations.Add(new Vector2(350, 300));
            wallLocations.Add(new Vector2(375, 275));
            wallLocations.Add(new Vector2(250, 100));

            wallLocations.Add(new Vector2(550, 225));
            wallLocations.Add(new Vector2(550, 125));
            wallLocations.Add(new Vector2(575, 125));

            wallLocations.Add(new Vector2(650, 200));

            foreach (Vector2 wall in wallLocations)
                Entities.Add(new Wall(Game, wall));
            foreach (Entity e in Entities)
                e.Initialize();
        }

        public static Vector2 PointToWorldSpace(
            Vector2 point,
            Vector2 heading,
            Vector2 side,
            Vector2 position)
        {        
            point = Vector2.Transform(
                point,
                new Matrix(
                    heading.X, heading.Y, 0, 0,
                    side.X, side.Y, 0, 0,
                    0, 0, 0, 0,
                    0, 0, 0, 0
            ));

            point = Vector2.Transform(
                point,
                Matrix.CreateTranslation(new Vector3(position.X, position.Y, 0))
            );

            return point;
        }

        #endregion
    }
}
