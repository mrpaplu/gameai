﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities
{
    public class Wall : Entity
    {
        public Wall(Game1 game, Vector2 position)
            : base(game, position)
        {
            texture = "wall";
        }
    }
}
