﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities
{
    public class Mine : Entity
    {
        #region Properties and fields

        public double Crystals { get; set; }

        public bool Depleted
        {
            get
            {
                return Crystals <= 0;
            }
        }

        #endregion

        #region Constructor

        public Mine(Game1 game, Vector2 position)
            : base(game, position)
        {
            texture = "crystals";
            Crystals = 1000;
        }

        #endregion

        #region DrawableGameComponent

        /// <summary>
        /// Don't draw empty mines
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if(!Depleted)
                base.Draw(gameTime);
        }

        #endregion
    }
}
