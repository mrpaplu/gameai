﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities
{
    public class Base : Entity
    {
        public Base(Game1 game, Vector2 position)
            : base(game, position)
        {
            texture = "base";
        }
    }
}
