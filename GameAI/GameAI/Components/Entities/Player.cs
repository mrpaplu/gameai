﻿using System.Collections.Generic;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Goals;
using GameAI.Components.Entities.Goals.Composite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameAI.Components.Entities
{
    public class Player : MovingEntity
    {
        private MouseState _previousMouseState;

        public Player(Game1 game, Vector2 position, Vector2 velocity)
            : base(game, position, velocity)
        {
            texture = "spaceship";
            MaxSpeed = 1;
            Mass = 30;
            Color = Color.Blue;
            Crystals = 0;
        }

        public double Crystals { get; set; }

        /// <summary>
        /// Allows the Game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();

            if (!mouseState.Equals(_previousMouseState))
            {
                Vector2 mousePosition = new Vector2(mouseState.X, mouseState.Y);
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    if (mouseState.X > 0 && mouseState.X < Game.Graphics.PreferredBackBufferWidth
                        && mouseState.Y > 0 &&
                        mouseState.Y < Game.Graphics.PreferredBackBufferHeight)
                    {
                        if(Goal != null)
                            Goal.Terminate(gameTime);
                        Goal = new MoveToLocation(Game, this, mousePosition);
                        Goal.Initialize();
                    }
                }
            }

            if(Goal != null)
                Goal.Process(gameTime);

            _previousMouseState = mouseState;

            base.Update(gameTime);
        }
    }
}
