﻿using GameAI.Components.Entities.Goals.Composite;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities
{
    public class Miner : MovingEntity
    {
        #region Properties and fields

        public double miningSpeed = 0.1f;

        public Mine Target { get; set; }

        private double _crystals;
        public double Crystals 
        {
            get
            {
                return _crystals;
            }
            set
            {
                _crystals = value;
                if (_crystals < 0)
                    _crystals = 0;
                if (_crystals > Capacity)
                    _crystals = Capacity;
            } 
        }

        public const float Capacity = 100;

        public bool Full
        {
            get
            {
                return Crystals >= Capacity;
            }
        }

        public bool Empty
        {
            get
            {
                return Crystals <= 0;
            }
        }

        #endregion

        #region Constructor

        public Miner(Game1 game, Vector2 position, Vector2 velocity)
            : base(game, position, velocity)
        {
            texture = "miner";
            Color = Color.Yellow;
            MaxSpeed = 1;
            Mass = 30;
        }

        #endregion

        #region DrawableGameComponent

        public override void Initialize()
        {
            Goal = new Harvest(Game, this);
            Goal.Initialize();

            base.Initialize();
        }

        #endregion
    }
}
