﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities
{
    public class Enemy : MovingEntity
    {
        public Enemy(Game1 game, Vector2 position, Vector2 velocity)
            : base(game, position, velocity)
        {
            texture = "spaceship";
            MaxSpeed = 1;
            Mass = 300;
            SteeringBehaviours.Wander.Active = true;
        }
    }
}