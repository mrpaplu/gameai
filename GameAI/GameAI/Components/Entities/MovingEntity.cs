﻿using System;
using ExtensionMethods;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Behaviours;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAI.Components.Entities
{
    public class MovingEntity : Entity
    {
        #region Properties and fields

        /// <summary>
        /// The maximum speed the entity can move with
        /// </summary>
        public float MaxSpeed { get; internal set; }

        /// <summary>
        /// The velocity of the entity
        /// </summary>
        public Vector2 Velocity { get; internal set; }

        /// <summary>
        /// The steeringbehaviours class holds the behaviours
        /// </summary>
        public SteeringBehaviours SteeringBehaviours { get; set; }

        /// <summary>
        /// The direction the entity is heading towards
        /// </summary>
        public Vector2 Heading { get; set; }

        /// <summary>
        /// The pendicular of heading
        /// </summary>
        public Vector2 Side { get; set; }

        /// <summary>
        /// The mass of the entity
        /// </summary>
        protected float Mass { get; set; }

        /// <summary>
        /// The maximum force
        /// </summary>
        protected double MaxForce { get; set; }

        /// <summary>
        /// The turnrate. Never used?
        /// </summary>
        protected double TurnRate { get; set; }

        /// <summary>
        /// The texture used for drawing debugging lines
        /// </summary>
        private Texture2D LineTexture { get; set; }

        /// <summary>
        /// The speed of the entity
        /// </summary>
        public float Speed
        {
            get
            {
                return Velocity.Length();
            }
        }

        #endregion

        #region Constructor

        public MovingEntity(Game1 game, Vector2 position, Vector2 velocity)
            : base(game, position)
        {
            SteeringBehaviours = new SteeringBehaviours(game, this);
            Velocity = velocity;
            Heading = new Vector2(Velocity.X + 0.00000001f, Velocity.Y);
            Side = Vector2Utilities.Perpendicular(Heading);
        }

        #endregion

        #region GameComponent

        /// <summary>
        /// Initializes texture and steeringbehaviours
        /// </summary>
        public override void Initialize()
        {
            LineTexture = ((GameComponent) this).Game.Content.Load<Texture2D>("line");
            SteeringBehaviours.Initialize();

            base.Initialize();
        }

        /// <summary>
        /// Updates the velocity, position, heading and side of the entity
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (Goal != null)
                Goal.Process(gameTime);

            Vector2 steeringForce = new Vector2(0);

            steeringForce += SteeringBehaviours.GetForce();

            Vector2 acceleration = steeringForce / Mass;
            Velocity += acceleration * gameTime.ElapsedGameTime.Milliseconds;

            // Update local coordinates
            if (Velocity.LengthSquared() > 0.00000001)
            {
                Heading = Vector2.Normalize(Velocity);
                Side = Vector2Utilities.Perpendicular(Heading);
            }

            Velocity.Truncate(MaxSpeed / 100);
            if (Velocity.LengthSquared() > 0.01)
                Position += Velocity * gameTime.ElapsedGameTime.Milliseconds / 10;

            Heading.Normalize();
            Side.Normalize();

            base.Update(gameTime);
        }

        /// <summary>
        /// Draws debugging info when debuggin is enabled
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            rotation = Vector2Utilities.Angle(Heading);

            if (Goal != null && Game.DebuggingGoals)
                Goal.Draw(gameTime);

            if (Game.DebuggingGraph)
            {
                Game.SpriteBatch.Begin();
                Game.SpriteBatch.DrawLine(LineTexture, Position, Position + (Vector2.Normalize(Heading) * 30), 1, Color.Red);
                Game.SpriteBatch.DrawLine(LineTexture, Position + (Vector2.Normalize(Side) * 30), Position - (Vector2.Normalize(Side) * 30), 1, Color.Red);
                Game.SpriteBatch.End();
            }

            SteeringBehaviours.Draw(gameTime);

            base.Draw(gameTime);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the time it should take to travel to the destination
        /// </summary>
        /// <param name="destination"></param>
        /// <returns></returns>
        public TimeSpan CalculateTimeToReachPosition(Vertex destination)
        {
            return TimeSpan.FromMilliseconds(Vector2.Distance(Position, destination.Location) / MaxSpeed);
        }

        /// <summary>
        /// Returns if the entity is at a certain location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public bool IsAtLocation(Vector2 location)
        {
            const double tolerance = 10.0;
            return Vector2.Distance(Position, location) < tolerance;
        }

        #endregion
    }
}
