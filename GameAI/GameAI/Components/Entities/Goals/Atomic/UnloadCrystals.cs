﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Atomic
{
    public class UnloadCrystals : Goal
    {
        public Base Base { get; set; }

        public new Miner Owner { get; set; }

        public Player Player
        {
            get
            {
                return (Player) Game.Services.GetService(typeof(Player));
            }
        }

        public UnloadCrystals(Game1 game, Miner owner, Base homeBase)
            : base(game, owner)
        {
            Base = homeBase;
            Owner = owner;
        }

        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            if (!Owner.Empty)
            {
                Owner.Crystals -= Owner.miningSpeed;
                Player.Crystals += Owner.miningSpeed;
            }
            else
            {
                Status = Status.Completed;
            }

            if (Owner.Full)
                Status = Status.Completed;

            return Status;
        }
    }
}
