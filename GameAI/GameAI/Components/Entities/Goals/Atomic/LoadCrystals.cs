﻿using System;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Atomic
{
    public class LoadCrystals : Goal
    {
        public Mine Mine { get; set; }

        public new Miner Owner { get; set; }

        public LoadCrystals(Game1 game, Miner owner, Mine mine)
            : base(game, owner)
        {
            Mine = mine;
            Owner = owner;
        }

        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            if (!Mine.Depleted)
            {
                Mine.Crystals -= Owner.miningSpeed;
                Owner.Crystals += Owner.miningSpeed;
            }
            else
            {
                Status = Status.Completed;
            }

            if (Owner.Full || Mine.Depleted)
                Status = Status.Completed;

            return Status;
        }
    }
}
