﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Atomic
{
    public class Flee : Goal
    {

        #region Properties and fields

        /// <summary>
        /// Are you sure this is safe?
        /// </summary>
        private const double SafeDistance = 25;

        /// <summary>
        /// Returns true if the bot's safe again
        /// </summary>
        bool IsSafe
        {
            get
            {
                if (Vector2.Distance(Owner.Position, FleeFrom.Position) > SafeDistance)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// The target to flee from
        /// </summary>
        Entity FleeFrom { get; set; }

        #endregion

        #region Constructor

        public Flee(Game1 game, MovingEntity owner, Entity fleeFrom)
            : base(game, owner)
        {
            FleeFrom = fleeFrom;
        }

        #endregion

        #region Goal

        /// <summary>
        /// Activates the flee behaviour
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Activate(GameTime gameTime)
        {
            DeactivateSteeringBehaviours();
            Owner.SteeringBehaviours.Flee.Target = FleeFrom.Position;
            Owner.SteeringBehaviours.Flee.Active = true;
        }

        /// <summary>
        /// Returns complete when bot's safe again
        /// </summary>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            if (IsSafe)
                Status = Status.Completed;

            return Status;
        }

        /// <summary>
        /// Terminates the flee behaviour
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Terminate(GameTime gameTime)
        {
            Owner.SteeringBehaviours.Flee.Active = false;
            base.Terminate(gameTime);
        }

        #endregion
    }
}
