﻿using System;
using System.Diagnostics;
using GameAI.Components.Datastructures;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Atomic
{
    public class TraverseEdge : Goal
    {
        #region Fields and properties

        /// <summary>
        /// The edge to traverse
        /// </summary>
        Edge Edge { get; set; }

        /// <summary>
        /// If this edge is the last in it's path
        /// </summary>
        bool LastEdgeInPath { get; set; }

        /// <summary>
        /// The time it should take to traverse the path
        /// </summary>
        TimeSpan TimeExpected { get; set; }

        /// <summary>
        /// The time elapsed according to GameTime
        /// </summary>
        TimeSpan ActualTimeElapsed { get; set; }

        /// <summary>
        /// Has the bot been dumb?
        /// </summary>
        bool IsStuck
        {
            get
            {
                return false;
                //return ActualTimeElapsed > TimeExpected;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Traverses the edge in a path
        /// </summary>
        /// <param name="owner">The owner</param>
        /// <param name="edge">The edge to traverse</param>
        /// <param name="lastEdge">Is this the last edge in the path?</param>
        public TraverseEdge(Game1 game, MovingEntity owner, Edge edge, bool lastEdge)
            : base(game, owner)
        {
            Edge = edge;
            LastEdgeInPath = lastEdge;
        }

        public override string ToString()
        {
            string toString = "";
            if (HasFailed)
                toString += "FAILED!";
            if (IsComplete)
                toString += "COMPLETE!";
            if (IsActive)
                toString += "ACTIVE!";
            if (IsInActive)
                toString += "INACTIVE!";

            if (LastEdgeInPath)
                toString += "Arriving at ";
            else if (IsStuck)
                toString += "Stuck at ";
            else
            {
                toString += "Traversing edge from ";
                toString += "(" + Edge.Source.Location.X + "," + Edge.Source.Location.Y + ")";
                toString += " to ";
            }
            toString += "(" + Edge.Destination.Location.X + "," + Edge.Destination.Location.Y + ")\n";
            return toString;
        }

        #endregion

        #region Goal

        /// <summary>
        /// Activates the goal and starts counting stupidity of the bot
        /// </summary>
        public override void Activate(GameTime gameTime)
        {
            // Set active
            Status = Status.Active;

            // Set starting time and expected time
            //TimeExpected = Owner.CalculateTimeToReachPosition(Edge.Destination);
            //const double marginOfError = 2.0;
            //TimeExpected = TimeExpected.Add(TimeSpan.FromSeconds(marginOfError));

            // Activate behaviours
            if (LastEdgeInPath)
            {
                DeactivateSteeringBehaviours();
                Owner.SteeringBehaviours.Arrive.Target = Edge.Destination.Location;
                Owner.SteeringBehaviours.Arrive.Active = true;
            }
            else
            {
                DeactivateSteeringBehaviours();
                Owner.SteeringBehaviours.Seek.Target = Edge.Destination.Location;
                Owner.SteeringBehaviours.Seek.Active = true;
            }
        }

        /// <summary>
        /// Process the goal
        /// </summary>
        /// <returns></returns>
        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            ActualTimeElapsed += gameTime.ElapsedGameTime;

            // Fail if stuck
            if (IsStuck)
                Status = Status.Failed;

            // Destination found? Yeah!
            if (Owner.IsAtLocation(Edge.Destination.Location))
                Status = Status.Completed;

            return Status;
        }

        /// <summary>
        /// Terminate the traversing
        /// </summary>
        public override void Terminate(GameTime gameTime)
        {
            Owner.SteeringBehaviours.Seek.Active = false;
            //Owner.SteeringBehaviours.Arrive.Active = false;

            base.Terminate(gameTime);
        }

        #endregion
    }
}
