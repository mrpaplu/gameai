﻿using System;
using GameAI.Components.Entities.Goals.Composite;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Evaluators
{
    public class MoveToLocationEvaluator : GoalEvaluator
    {
        #region Constructor
        
        public MoveToLocationEvaluator(Game1 game, MovingEntity entity)
            : base(game, entity)
        {
        }

        #endregion

        #region GoalEvaluator

        /// <summary>
        /// Returns the desirability score of MoveToLocation
        /// </summary>
        /// <returns></returns>
        public override double GetScore()
        {
            return 0.7;
        }

        /// <summary>
        /// Activates MoveToLocation
        /// </summary>
        public override void Activate(GameTime gameTime)
        {
            Goal goal = new MoveToLocation(Game, Entity, new Vector2(400, 400));
            goal.Initialize();
            Entity.Goal.AddSubGoal(goal);
        }

        #endregion
    }
}
