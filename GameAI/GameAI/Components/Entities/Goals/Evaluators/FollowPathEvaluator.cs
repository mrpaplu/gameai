﻿using System;
using System.Collections.Generic;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Goals.Composite;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Evaluators
{
    public class FollowPathEvaluator : GoalEvaluator
    {
        #region Constructor 

        public FollowPathEvaluator(Game1 game, MovingEntity entity)
            : base(game, entity)
        {
        }

        #endregion

        #region GoalEvaluator

        /// <summary>
        /// Returns the desirability score of FollowPath
        /// </summary>
        /// <returns></returns>
        public override double GetScore()
        {
            return 0.5;
        }

        /// <summary>
        /// Activates FollowPath
        /// </summary>
        public override void Activate(GameTime gameTime)
        {
            Vertex location = Entity.Game.World.Graph.GetNearestVertex(Entity.Position);
            Vertex destination = Entity.Game.World.Graph.GetNearestVertex(new Vector2(0, 0));
            Stack<Edge> path = Entity.Game.World.Graph.GetShortestPath(location, destination);
            Entity.Goal.AddSubGoal(new FollowPath(Game, Entity, path));
            Entity.Goal.Activate(gameTime);
        }

        #endregion
    }
}
