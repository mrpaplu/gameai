﻿using GameAI.Components.Entities.Goals.Composite;
using GameAI.Components.Fuzzy;
using GameAI.Components.Fuzzy.Terms;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Evaluators
{
    public class DeliverEvaluator : GoalEvaluator
    {
        FuzzyModule Fuzzy { get; set; }

        public Base Base
        {
            get
            {
                return (Base) Game.Services.GetService(typeof(Base));
            }
        }

        public new Miner Entity { get; set; }

        public DeliverEvaluator(Game1 game, Miner entity)
            : base(game, entity)
        {
            Entity = entity;
            Fuzzy = new FuzzyModule();

            FuzzyVariable crystals = Fuzzy.CreateFLV("Crystals");

            Set crystalsLow = crystals.AddLeftShoulder("CrystalsLow", 0, 25, 75);
            Set crystalsMedium = crystals.AddTriangular("CrystalsMedium", 25, 75, 100);
            Set crystalsHigh = crystals.AddRightShoulder("CrystalsHigh", 75, 100, 100);

            FuzzyVariable hasTarget = Fuzzy.CreateFLV("Target");

            Set hasNoTarget = hasTarget.AddLeftShoulder("TargetNo", 0, 0, 1);
            Set hasATarget = hasTarget.AddRightShoulder("TargetYes", 0, 1, 1);

            FuzzyVariable desirable = Fuzzy.CreateFLV("Desirability");

            Set unDesirable = desirable.AddLeftShoulder("UnDesirable", 0, 25, 50);
            Set desirableSet = desirable.AddTriangular("Desirable", 25, 50, 75);
            Set veryDesirable = desirable.AddRightShoulder("VeryDesirable", 50, 75, 100);

            Fuzzy.AddRule(new Or(hasNoTarget, new And(hasATarget, crystalsHigh)), veryDesirable);

            Fuzzy.AddRule(new And(hasATarget, crystalsMedium), desirableSet);
            Fuzzy.AddRule(new And(hasATarget, crystalsLow), unDesirable);

            Fuzzy.AddRule(crystalsLow, unDesirable);
            Fuzzy.AddRule(crystalsMedium, desirableSet);
            Fuzzy.AddRule(crystalsHigh, veryDesirable);
        }

        public override double GetScore()
        {
            Fuzzy.Fuzzify("Target", Entity.Target == null ? 0 : 1);
            Fuzzy.Fuzzify("Crystals", Entity.Crystals);

            return Fuzzy.Defuzzify("Desirability");
        }

        public override void Activate(GameTime gameTime)
        {
            Entity.Goal.AddSubGoal(new Deliver(Game, Entity, Base));
        }
    }
}
