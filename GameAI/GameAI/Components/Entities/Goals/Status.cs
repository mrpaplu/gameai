﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Components.Entities.Goals
{
    public enum Status
    {
        Active,
        Inactive,
        Completed,
        Failed,
        Terminated
    }
}
