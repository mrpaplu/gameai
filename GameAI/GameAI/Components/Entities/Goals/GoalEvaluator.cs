﻿using GameAI.Components.Entities.Goals.Composite;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals
{
    public abstract class GoalEvaluator
    {
        #region Properties

        /// <summary>
        /// The owner of this evaluator
        /// </summary>
        public MovingEntity Entity { get; set; }

        /// <summary>
        /// The game
        /// </summary>
        public Game1 Game { get; set; }

        #endregion

        #region Constructor

        protected GoalEvaluator(Game1 game, MovingEntity entity)
        {
            Game = game;
            Entity = entity;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the evaluation score
        /// </summary>
        /// <returns></returns>
        public abstract double GetScore();

        /// <summary>
        /// Activate the desired steering behaviours
        /// </summary>
        /// <param name="brain"></param>
        public abstract void Activate(GameTime gameTime);

        #endregion
    }
}
