﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals
{
    public abstract class CompositeGoal : Goal
    {
        #region Properties and fields

        protected List<Goal> SubGoals { get; set; }

        #endregion

        #region Constructor

        public CompositeGoal(Game1 game, MovingEntity owner)
            : base(game, owner)
        {
            SubGoals = new List<Goal>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Processes all subgoals
        /// </summary>
        /// <returns></returns>
        protected Status ProcessSubgoals(GameTime gameTime)
        {
            // Terminate and remove all failed and completed subgoals
            foreach (Goal goal in SubGoals.Where(x => x.HasFailed || x.IsComplete).ToList())
            {
                goal.Terminate(gameTime);
            }
            SubGoals.RemoveAll(x => x.HasFailed || x.IsComplete || x.IsTerminated);


            // List empty? Goal completed. Else process the first subgoal
            if (!SubGoals.Any())
                Status = Status.Completed;
            else
                Status = SubGoals.First().Process(gameTime);

            // First subgoal is active, so keep going
            if (Status == Status.Completed && SubGoals.Count() > 1)
                Status = Status.Active;

            return Status;
        }

        /// <summary>
        /// Add a subgoal to the list
        /// </summary>
        /// <param name="goal"></param>
        public override void AddSubGoal(Goal goal)
        {
            SubGoals.Insert(0, goal);
        }

        /// <summary>
        /// Terminate and clear all subgoals
        /// </summary>
        public void ClearAllSubGoals(GameTime gameTime)
        {
            foreach(Goal g in SubGoals)
                g.Terminate(gameTime);
            SubGoals.Clear();
        }

        /// <summary>
        /// Terminate all subgoals
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Terminate(GameTime gameTime)
        {
            ClearAllSubGoals(gameTime);
            base.Terminate(gameTime);
        }

        #endregion
    }
}
