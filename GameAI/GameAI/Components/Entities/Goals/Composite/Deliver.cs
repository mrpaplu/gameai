﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Goals.Atomic;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    public class Deliver : CompositeGoal
    {
        public Base Base { get; set; }

        public new Miner Owner { get; set; }

        public Deliver(Game1 game, Miner owner, Base homeBase)
            : base(game, owner)
        {
            Base = homeBase;
            Owner = owner;
        }

        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            if (!Owner.IsAtLocation(Base.Position))
            {
                SubGoals.Add(new MoveToLocation(Game, Owner, Base.Position));
            }
            else
            {
                SubGoals.Add(new UnloadCrystals(Game, Owner, Base));
            }
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            // Get status from subgoals
            Status = ProcessSubgoals(gameTime);

            // Done with edge? Activate and grab new edge
            if (Status == Status.Completed && !Owner.Empty)
                Activate(gameTime);

            return Status;
        }
    }
}
