﻿using System;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Goals.Atomic;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    public class MineCrystals : CompositeGoal
    {
        public Mine Mine { get; set; }

        public new Miner Owner { get; set; }

        public MineCrystals(Game1 game, Miner owner, Mine mine)
            : base(game, owner)
        {
            Mine = mine;
            Owner = owner;
        }

        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            if (!Owner.IsAtLocation(Mine.Position))
            {
                Vertex position = Graph.GetNearestVertex(Owner.Position);
                Vertex destination = Graph.GetNearestVertex(Mine.Position);

                SubGoals.Add(new FollowPath(Game,Owner,Graph.GetShortestPath(position, destination)));
            }
            else
            {
                SubGoals.Add(new LoadCrystals(Game, Owner, Mine));
            }
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            // Get status from subgoals
            Status = ProcessSubgoals(gameTime);

            // Done with edge? Activate and grab new edge
            if (Status == Status.Completed && !Mine.Depleted && !Owner.Full)
                Activate(gameTime);

            if (Mine.Depleted)
                Owner.Target = null;

            return Status;
        }
    }
}
