﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Goals.Atomic;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    public class Explore : CompositeGoal
    {
        #region Fields and properties

        /// <summary>
        /// The center of the area we will explore
        /// </summary>
        /// 
        public Vector2 Area { get; set; }
        /// <summary>
        /// What are we looking for?
        /// </summary>
        public Type LookingFor { get; set; }

        /// <summary>
        /// Offset of the search
        /// </summary>
        public float Offset { get; set; }

        /// <summary>
        /// The found entity
        /// </summary>
        public Mine Target { get; set; }

        /// <summary>
        /// The harvest using this Explore
        /// </summary>
        public Harvest Parent { get; set; }

        /// <summary>
        /// The path the explore will follow
        /// </summary>
        Stack<Edge> Path { get; set; }

        #endregion

        #region Constructor

        public Explore(Game1 game, MovingEntity owner, Vector2 area, float offset, Harvest parent)
            : base(game, owner)
        {
            Area = area;
            Offset = offset;
            Parent = parent;
            Path = new Stack<Edge>();
        }

        public override string ToString()
        {
            string toString = "";
            if (HasFailed)
                toString += "FAILED!";
            if (IsComplete)
                toString += "COMPLETE!";
            if (IsActive)
                toString += "ACTIVE!";
            if (IsInActive)
                toString += "INACTIVE!";
            toString += "Exploring (" + Area.X + "," + Area.Y + ")\n";

            foreach (Goal goal in SubGoals)
            {
                toString += goal;
            }
            Console.WriteLine(toString);
            return toString;
        }

        #endregion

        #region CompositeGoal

        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            if (!Path.Any())
            {
                if (Graph.Vertices.Count <= 0)
                    return;
                Vertex topleft =
                    Graph.GetNearestVertex(new Vector2(Area.X - Offset / 2, Area.Y - Offset / 2));
                Vertex topright =
                    Graph.GetNearestVertex(new Vector2(Area.X + Offset / 2, Area.Y - Offset / 2));
                Vertex bottomleft =
                    Graph.GetNearestVertex(new Vector2(Area.X - Offset / 2, Area.Y + Offset / 2));
                Vertex bottomright =
                    Graph.GetNearestVertex(new Vector2(Area.X + Offset / 2, Area.Y + Offset / 2));
                Vertex position = Graph.GetNearestVertex(Owner.Position);
                Vertex center = Graph.GetNearestVertex(Area);

                Path.Clear();
                foreach (Edge e in Graph.GetShortestPath(topleft, center).Reverse())
                    Path.Push(e);
                foreach (Edge e in Graph.GetShortestPath(bottomleft, topleft).Reverse())
                    Path.Push(e);
                foreach (Edge e in Graph.GetShortestPath(bottomright, bottomleft).Reverse())
                    Path.Push(e);
                foreach (Edge e in Graph.GetShortestPath(topright, bottomright).Reverse())
                    Path.Push(e);
                foreach (Edge e in Graph.GetShortestPath(topleft, topright).Reverse())
                    Path.Push(e);
                foreach (Edge e in Graph.GetShortestPath(position, topleft).Reverse())
                    Path.Push(e);
            }

            // Add subgoal to go to first edge
            if (Path.Any())
            {
                Edge edge = Path.Pop();
                AddSubGoal(new TraverseEdge(Game, Owner, edge, !Path.Any()));
            }
            else
            {
                Terminate(gameTime);
            }
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            // If we find a mine 
            if (Path.Any())
            {
                World.TagObstaclesInRangeOf(Owner, 50f);
                Target = (Mine) World.Entities.FirstOrDefault(x => x.Tagged && x.GetType() == typeof(Mine));
                if (Target != null && !Target.Depleted)
                {
                    Edge location = Path.FirstOrDefault();
                    Path.Clear();
                    Path.Push(location);
                    Parent.Target = Target;
                    Status = Status.Completed;
                }
            }

            // Get status from subgoals
            Status = ProcessSubgoals(gameTime);

            // Done with edge? Activate and grab new edge
            if (Status == Status.Completed && Path.Any())
            {
                Activate(gameTime);
            }

            return Status;
        }

        #endregion
    }
}
