﻿using System.Collections.Generic;
using GameAI.Components.Datastructures;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    public class MoveToLocation : CompositeGoal
    {
        #region Properties and fields

        /// <summary>
        /// Where 're we heading?
        /// </summary>
        Vector2 Destination { get; set; }

        #endregion

        #region Constructor

        public MoveToLocation(Game1 game, MovingEntity owner, Vector2 position)
            : base(game, owner)
        {
            Destination = position;
        }
        
        public override string ToString()
        {
            string toString = "";
            if (HasFailed)
                toString += "FAILED!";
            if (IsComplete)
                toString += "COMPLETE!";
            if (IsActive)
                toString += "ACTIVE!";
            if (IsInActive)
                toString += "INACTIVE!";
                
            toString += "Moving to location (" + Destination.X + "," + Destination.Y + ")\n";
            foreach (Goal goal in SubGoals)
            {
                toString += goal;
            }
            return toString;
        }

        #endregion

        #region CompositeGoal

        /// <summary>
        /// Activate path finding
        /// </summary>
        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            Vertex source = Graph.GetNearestVertex(Owner.Position);
            Vertex destination = Graph.GetNearestVertex(Destination);

            Stack<Edge> path = Graph.GetShortestPath(source, destination);

            SubGoals.Add(new FollowPath(Game, Owner, path));
        }

        /// <summary>
        /// Process path finding
        /// </summary>
        /// <returns></returns>
        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            Status = ProcessSubgoals(gameTime);

            ReactivateIfFailed(gameTime);

            return Status;
        }

        #endregion
    }
}
