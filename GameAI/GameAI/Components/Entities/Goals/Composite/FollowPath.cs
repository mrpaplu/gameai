﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAI.Components.Datastructures;
using GameAI.Components.Entities.Goals.Atomic;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    public class FollowPath : CompositeGoal
    {
        #region Fields and properties

        Stack<Edge> Path { get; set; }

        #endregion

        #region Constructor

        public FollowPath(Game1 game, MovingEntity owner, Stack<Edge> path)
            : base(game, owner)
        {
            Path = path;
        }

        public override string ToString()
        {
            string toString = "";
            if (HasFailed)
                toString += "FAILED!";
            if (IsComplete)
                toString += "COMPLETE!";
            if (IsActive)
                toString += "ACTIVE!";
            if (IsInActive)
                toString += "INACTIVE!";
            toString += "Following path\n";

            foreach (Goal goal in SubGoals)
            {
                toString += goal;
            }
            Console.WriteLine(toString);
            return toString;
        }

        #endregion

        #region CompositeGoal

        /// <summary>
        /// Activates path following and adds subgoal for first edge in path
        /// </summary>
        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            // Add subgoal to go to first edge
            if (Path.Any())
            {
                Edge edge = Path.Pop();
                AddSubGoal(new TraverseEdge(Game, Owner, edge, !Path.Any()));
            }
            else
            {
                Terminate(gameTime);
            }
        }

        /// <summary>
        /// Process the goal
        /// </summary>
        /// <returns></returns>
        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            // Get status from subgoals
            Status = ProcessSubgoals(gameTime);

            // Done with edge? Activate and grab new edge
            if (Status == Status.Completed && Path.Any())
            {
                Activate(gameTime);
            }

            return Status;
        }

        #endregion
    }
}
