﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GameAI.Components.Entities.Goals.Evaluators;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    public class Harvest : CompositeGoal
    {
        List<GoalEvaluator> Evaluators { get; set; }
        
        public Player Player { get; set; }

        public Mine Target
        {
            get
            {
                return Owner.Target;
            }
            set
            {
                Owner.Target = value;
                MineEvaluator eval =
                    (MineEvaluator)Evaluators.FirstOrDefault(x => x.GetType() == typeof(MineEvaluator));
                if (eval != null)
                    eval.Target = Owner.Target;
            }
        }

        public new Miner Owner { get; set; }

        public Harvest(Game1 game, Miner owner)
            : base(game, owner)
        {
            Owner = owner;
            Evaluators = new List<GoalEvaluator>();
            Evaluators.Add(new MineEvaluator(Game, owner));
            Evaluators.Add(new DeliverEvaluator(Game, owner));
            Evaluators.Add(new ExploreEvaluator(Game, Owner));
        }

        public override string ToString()
        {
            string toString = "";
            if (HasFailed)
                toString += "FAILED!";
            if (IsComplete)
                toString += "COMPLETE!";
            if (IsActive)
                toString += "ACTIVE!";
            if (IsInActive)
                toString += "INACTIVE!";
            toString += "Harvesting\n";

            foreach (Goal goal in SubGoals)
            {
                toString += goal;
            }
            Console.WriteLine(toString);
            return toString;
        }

        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            if (Evaluators.Any())
            {
                GoalEvaluator best = Evaluators.First();
                    double bestScore = best.GetScore();

                // Pick best
                foreach (GoalEvaluator eval in Evaluators)
                {
                        double evalScore = eval.GetScore();
                        if (evalScore > bestScore)
                        	best = eval;
                }
                best.Activate(gameTime);
            }
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            Status = ProcessSubgoals(gameTime);

            ReactivateIfFailed(gameTime);

            if (Status == Status.Completed)
                Activate(gameTime);

            return Status;
        }
    }
}
