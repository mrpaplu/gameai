﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameAI.Components.Entities.Goals.Evaluators;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Goals.Composite
{
    /// <summary>
    /// Think is the brain of a bot
    /// </summary>
    public class Think : CompositeGoal
    {
        #region Properties and fields

        private List<GoalEvaluator> Evaluators { get; set; }

        #endregion

        #region Constructor

        public Think(Game1 game, MovingEntity owner)
            : base(game, owner)
        {
            Evaluators = new List<GoalEvaluator>();
            Evaluators.Add(new FollowPathEvaluator(Game, owner));
            Evaluators.Add(new MoveToLocationEvaluator(Game, owner));
        }

        public override string ToString()
        {
            string toString = "";
            if (HasFailed)
                toString += "FAILED!";
            if (IsComplete)
                toString += "COMPLETE!";
            if (IsActive)
                toString += "ACTIVE!";
            if (IsInActive)
                toString += "INACTIVE!";

            toString += " Thinking\n";
            foreach (Goal goal in SubGoals)
            {
                toString += goal;
            }
            return toString;
        }

        #endregion

        #region CompositeGoal

        /// <summary>
        /// Not much to it
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Activate(GameTime gameTime)
        {
            Status = Status.Active;

            ClearAllSubGoals(gameTime);

            if (Evaluators.Any())
            {
                GoalEvaluator best = Evaluators.First();

                // Pick best
                foreach (GoalEvaluator eval in Evaluators)
                {
                    if (eval.GetScore() > best.GetScore())
                        best = eval;
                }
                best.Activate(gameTime);
            }
        }

        public override Status Process(GameTime gameTime)
        {
            ActivateIfInactive(gameTime);

            Status = ProcessSubgoals(gameTime);

            ReactivateIfFailed(gameTime);

            if (Status == Status.Completed)
                Activate(gameTime);

            return Status;
        }

        public override void Terminate(GameTime gameTime)
        {
            foreach(Goal g in SubGoals)
                g.Terminate(gameTime);
            base.Terminate(gameTime);
        }

        #endregion
    }
}
