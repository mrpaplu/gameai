﻿using System;
using GameAI.Components.Datastructures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAI.Components.Entities.Goals
{
    public abstract class Goal : DrawableGameComponent
    {
        #region Properties and fields

        /// <summary>
        /// The owner of the goal
        /// </summary>
        protected MovingEntity Owner { get; set; }

        /// <summary>
        /// Get the game
        /// </summary>
        public new Game1 Game { get; set; }

        /// <summary>
        /// Returns the graph service
        /// </summary>
        public Graph Graph
        {
            get
            {
                return (Graph) Game.Services.GetService(typeof(Graph));
            }
        }

        /// <summary>
        /// Return the world service
        /// </summary>
        public World World
        {
            get
            {
                return (World) Game.Services.GetService(typeof(World));
            }
        }

        /// <summary>
        /// The status of the goal
        /// </summary>
        protected Status Status { get; set; }

        /// <summary>
        /// Returns if this goal is active
        /// </summary>
        public bool IsActive
        {
            get
            {
                return Status == Status.Active;
            }
        }

        /// <summary>
        /// Returns if this goal is inactive
        /// </summary>
        public bool IsInActive
        {
            get
            {
                return Status == Status.Inactive;
            }
        }

        /// <summary>
        /// Returns whether or not this goal is complete
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return Status == Status.Completed;
            }
        }

        /// <summary>
        /// Returns whether or not this goal has failed
        /// </summary>
        public bool HasFailed
        {
            get
            {
                return Status == Status.Failed;
            }
        }

        /// <summary>
        /// Determines if the goal has been terminated
        /// </summary>
        public bool IsTerminated
        {
            get
            {
                return Status == Status.Terminated;
            }
        }

        /// <summary>
        /// Spritefont for debugging
        /// </summary>
        private SpriteFont _spriteFont;

        #endregion

        #region Constructor

        protected Goal(Game1 game, MovingEntity owner)
            : base(game)
        {
            Game = game;
            Owner = owner;
            Status = Status.Inactive;
        }

        #endregion

        #region DrawableGameComponent

        public override void Initialize()
        {
            _spriteFont = Game.Content.Load<SpriteFont>("StatisticsFont");
            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            string info = ToString();
            Game.SpriteBatch.Begin();
            Game.SpriteBatch.DrawString(
                _spriteFont,
                info,
                new Vector2(Owner.Position.X, Owner.Position.Y - 50),
                Color.Blue,
                0,
                new Vector2(80, 25),
                0.7f,
                SpriteEffects.None,
                1
                );
            Game.SpriteBatch.End();

            base.Draw(gameTime);
        }

        #endregion

        #region Abstract and virtual methods

        public abstract void Activate(GameTime gameTime);
        public abstract Status Process(GameTime gameTime);

        /// <summary>
        /// Don't forget to call base.Terminate()!
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Terminate(GameTime gameTime)
        {
            Status = Status.Terminated;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Activates the goal when inactive through it's own activation method
        /// </summary>
        protected void ActivateIfInactive(GameTime gameTime)
        {
            if (IsInActive)
            {
                Activate(gameTime);
            }
        }

        /// <summary>
        /// Activates the goal when failed through it's own activation method
        /// </summary>
        protected void ReactivateIfFailed(GameTime gameTime)
        {
            if (HasFailed)
            {
                Activate(gameTime);
            }
        }

        /// <summary>
        /// Prevent subgoals from being added to an atomic goal
        /// </summary>
        public virtual void AddSubGoal(Goal goal)
        {
            throw new Exception("Can't add subgoal to atomic goals.");
        }

        /// <summary>
        /// Deactivate steering behaviours to prevent them from interfering
        /// </summary>
        public void DeactivateSteeringBehaviours()
        {
            Owner.SteeringBehaviours.Seek.Active = false;
            Owner.SteeringBehaviours.Arrive.Active = false;
            Owner.SteeringBehaviours.Flee.Active = false;
        }

        #endregion
    }
}
