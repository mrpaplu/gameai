﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public class SteeringBehaviours : AbstractSteeringBehaviour
    {
        #region Behaviours

        public Arrive Arrive { get; set; }
        public Seek Seek { get; set; }
        public FollowPath FollowPath { get; set; }
        public Flee Flee { get; set; }
        public Wander Wander { get; set; }

        #endregion

        #region Constructor

        public SteeringBehaviours(Game1 game, MovingEntity e)
            : base(game, e)
        {
            Arrive = new Arrive(Game, Entity);
            Seek = new Seek(Game, Entity);
            FollowPath = new FollowPath(Game, Entity);
            Flee = new Flee(Game, Entity);
            Wander = new Wander(Game, Entity);
            Game.Components.Add(FollowPath);
        }

        #endregion

        #region AbstractSteeringBehaviour

        public override Vector2 GetForce()
        {
            Vector2 steeringForce = Vector2.Zero;

            steeringForce += Arrive.GetForce();
            steeringForce += Seek.GetForce();
            steeringForce += Flee.GetForce();
            steeringForce += Wander.GetForce();

            return steeringForce;
        }

        #endregion
    }
}
