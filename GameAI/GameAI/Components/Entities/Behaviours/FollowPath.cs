﻿using System.Collections.Generic;
using System.Linq;
using GameAI.Components.Datastructures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAI.Components.Entities.Behaviours
{
    public class FollowPath : AbstractSteeringBehaviour
    {
        #region Properties and fields

        private Texture2D _dijkstraDot;
        public Seek Seek { get; set; }
        private Arrive Arrive { get; set; }

        private Vertex CurrentWaypoint { get; set; }
        private const float WaypointSeekDistance = 1;
        private bool FoundDestination { get; set; }

        private Stack<Vertex> _path; 
        public Stack<Vertex> Path {
            get
            {
                return _path;
            }
            set
            {
                _path = value;
                FoundDestination = false;
                CurrentWaypoint = null;
            }
        }

        #endregion

        public FollowPath(Game1 game, MovingEntity e)
            : base(game, e)
        {
            Seek = new Seek(game, e);
            Arrive = new Arrive(game, e);
            Seek.Active = true;
            Arrive.Active = true;
            DrawOrder = 11;
        }

        public override void Initialize()
        {
            _dijkstraDot = Game.Content.Load<Texture2D>("dijkstradot");
            base.Initialize();
        }

        public override Vector2 GetForce()
        {
            if (FoundDestination)
                return Arrive.GetForce();

            // No path set?
            if (Path == null || !Path.Any() || !Active)
                return Vector2.Zero;

            // Only one left? Arrive to finish
            if (Path.Count() <= 1)
            {
                Arrive.Target = Path.Pop().Location;
                FoundDestination = true;
                return Arrive.GetForce();
            }

            // If close, set next waypoint
            if (CurrentWaypoint == null || Vector2.Distance(Entity.Position, CurrentWaypoint.Location) <
                WaypointSeekDistance)
            {
                CurrentWaypoint = Path.Pop();
            }

            Seek.Target = CurrentWaypoint.Location;
            return Seek.GetForce();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            if (Path != null && Path.Any() && Game.DebuggingGraph)
            {
                Game.SpriteBatch.Begin();
                foreach (Vertex vertex in Path)
                {
                    Game.SpriteBatch.Draw(
                                          _dijkstraDot,
                        vertex.Location,
                        null,
                        Color.Red,
                        0,
                        new Vector2(
                            (float) (_dijkstraDot.Width / 2) - 1,
                            (float) (_dijkstraDot.Height / 2) - 1),
                        0.7f,
                        SpriteEffects.None,
                        1);
                }
                Game.SpriteBatch.End();
            }
        }
    }
}
