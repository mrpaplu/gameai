﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public class Seek : AbstractSteeringBehaviour
    {
        public Vector2 Target { get; set; }

        public Seek(Game1 game, MovingEntity e)
            : base(game, e)
        {
        }

        public override Vector2 GetForce()
        {
            if (!Active)
                return Vector2.Zero;

            Vector2 desiredVelocity = Target - Entity.Position;
            if (desiredVelocity != Vector2.Zero)
                desiredVelocity.Normalize();
            desiredVelocity *= Entity.MaxSpeed;

            return desiredVelocity - Entity.Velocity;
        }
    }
}
