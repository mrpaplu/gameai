﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public class Flee : AbstractSteeringBehaviour
    {
        public Vector2 Target { get; set; }
        public float PanicDistance { get; set; }

        public Flee(Game1 game, MovingEntity movingEntity)
            : base(game, movingEntity)
        {
            PanicDistance = 100;
        }

        public override Vector2 GetForce()
        {
            if (!Active)
                return Vector2.Zero;

            if (Vector2.Distance(Entity.Position, Target) < PanicDistance)
            {
                return Vector2.Zero;
            }

            Vector2 desiredVelocity = Entity.Position - Target;
            if (desiredVelocity != Vector2.Zero)
                desiredVelocity.Normalize();
            desiredVelocity *= Entity.MaxSpeed;

            return desiredVelocity - Entity.Velocity;
        }
    }
}
