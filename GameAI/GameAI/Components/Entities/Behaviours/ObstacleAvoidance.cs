﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public class ObstacleAvoidance : AbstractSteeringBehaviour
    {
        #region Properties and fields

        private const float _minBoxLength = 1;

        private float BoxLength
        {
            get
            {
                return _minBoxLength
                + (Entity.Speed / Entity.MaxSpeed)
                * _minBoxLength;
            }
        }

        #endregion

        #region Constructor

        public ObstacleAvoidance(Game1 game, MovingEntity e)
            : base(game, e)
        {
        }

        #endregion

        #region Steeringbehaviour

        /// <summary>
        /// Sets the detectionbox and returns the force to avoid obstacles
        /// </summary>
        /// <returns></returns>
        public override Vector2 GetForce()
        {
            Vector2 force = Vector2.Zero;
            Entity closestIntersectingObstacle = null;
            float distanceToCib = float.MaxValue;
            Vector2 localPositionOfCib;

            Game.World.TagObstaclesInRangeOf(Entity, BoxLength);

            foreach (Entity e in Game.World.Entities)
            {
                if (!e.Tagged) break;

                Vector2 localPos = Vector2.Zero;
            }

            return force;
        }

        #endregion
    }
}
