﻿using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public abstract class AbstractSteeringBehaviour : DrawableGameComponent
    {
        public MovingEntity Entity { get; set; }
        public bool Active { get; set; }
        public new Game1 Game { get; set; }

        protected AbstractSteeringBehaviour(Game1 game, MovingEntity e)
            : base(game)
        {
            Entity = e;
            Game = game;
            Active = false;
        }

        public abstract Vector2 GetForce();
    }
}
