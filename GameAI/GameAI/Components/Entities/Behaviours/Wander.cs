﻿using System;
using ExtensionMethods;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public class Wander : AbstractSteeringBehaviour
    {
        private const int Radius = 2;
        private const int Jitter = 1;
        private const int Distance = 1;
        private Vector2 Target { get; set; }

        public Wander(Game1 game, MovingEntity e)
            : base(game, e)
        {
        }

        public override Vector2 GetForce()
        {
            if (!Active)
                return Vector2.Zero;

            Random rand = new Random();
            Target += new Vector2(rand.Next(0, 1000) * Jitter, rand.Next(0, 1000) * Jitter);

            Target = Target.Truncate(1);
            Target *= Radius;

            Vector2 targetLocal = Target + new Vector2(Distance, 0);

            Vector2 targetWorld = World.PointToWorldSpace(
                targetLocal,
                Entity.Heading,
                Entity.Side,
                Entity.Position
            );

            return Target - Entity.Position;
        }
    }
}
