﻿using System;
using Microsoft.Xna.Framework;

namespace GameAI.Components.Entities.Behaviours
{
    public class Arrive : AbstractSteeringBehaviour
    {
        public Vector2 Target { get; set; }
        public float Deceleration { get; set; }

        public Arrive(Game1 game, MovingEntity e)
            : base(game, e)
        {
            Deceleration = 3;
        }

        public override Vector2 GetForce()
        {
            if (!Active)
                return Vector2.Zero;

            Vector2 toTarget = Target - Entity.Position;

            float dist = toTarget.Length();

            if (dist > 0)
            {
                float speed = dist / (Deceleration * 5);

                speed = Math.Min(speed, Entity.MaxSpeed);

                Vector2 desiredVelocity = toTarget * speed / dist;

                return (desiredVelocity - Entity.Velocity);
            }
            return Vector2.Zero;
        }
    }
}
