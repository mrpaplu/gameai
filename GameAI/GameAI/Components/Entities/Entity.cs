﻿using GameAI.Components.Entities.Goals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAI.Components.Entities
{
    /// <summary>
    /// This is a Game component that implements IUpdateable.
    /// </summary>
    public abstract class Entity : DrawableGameComponent
    {
        #region Properties and fields

        /// <summary>
        /// Tagged for obstacle avoidance
        /// </summary>
        public bool Tagged { get; set; }

        /// <summary>
        /// The position of the entity in the Game world
        /// </summary>
        public Vector2 Position { get; internal set; }

        /// <summary>
        /// The size of the entity
        /// </summary>
        public Vector2 Size
        {
            get
            {
                if (_size != Vector2.Zero)
                    return _size;
                if (Texture != null)
                    return new Vector2(Texture.Width, Texture.Height);
                return Vector2.Zero;
            }
            set
            {
                _size = value;
            }
        }

        /// <summary>
        /// The Game
        /// </summary>
        public new Game1 Game { get; set; }

        /// <summary>
        /// The string used for loading the texture
        /// </summary>
        protected string texture = "";

        /// <summary>
        /// The texture
        /// </summary>
        protected Texture2D Texture { get; set; }

        /// <summary>
        /// The color of the entity
        /// </summary>
        protected Color Color { get; set; }

        /// <summary>
        /// The rotation in radians
        /// </summary>
        protected float rotation;

        /// <summary>
        /// The size of the entity
        /// </summary>
        private Vector2 _size = Vector2.Zero;

        /// <summary>
        /// The entity's goal
        /// </summary>
        public Goal Goal { get; set; }

        #endregion

        #region Constructor

        protected Entity(Game1 game, Vector2 position)
            : base(game)
        {
            Color = Color.White;
            Game = game;
            Position = position;
            rotation = 0;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns if the entity is covering a certain location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public bool CoversLocation(Vector2 location)
        {
            if (location.X >= Position.X - Size.X
                && location.Y >= Position.Y - Size.X
                && location.X <= Position.X + Size.X
                && location.Y <= Position.Y + Size.Y)
                return true;
            return false;
        }

        #endregion

        #region DrawableGameComponent

        /// <summary>
        /// Initializes the texture
        /// </summary>
        public override void Initialize()
        {
            Texture = ((GameComponent) this).Game.Content.Load<Texture2D>(texture);

            base.Initialize();
        }

        /// <summary>
        /// Draws the entity in the gameworld
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            Game.SpriteBatch.Begin();
            Game.SpriteBatch.Draw(
                Texture,
                Position,
                null,
                Color,
                rotation,
                new Vector2((float)(Texture.Width / 2) - 1, (float)(Texture.Height / 2) - 1),
                1,
                SpriteEffects.None,
                1
            );
            Game.SpriteBatch.End();

            base.Draw(gameTime);
        }

        #endregion
    }
}
