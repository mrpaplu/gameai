﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace GameAI.Datastructures
{
    public class Vertex
    {
        public List<Edge> Adjacent { get; protected set; }
        public Vector2 Location { get; set; }

        public Vertex Previous { get; set; }
        public bool Visited { get; set; }
        public float Distance { get; set; }

        public Vertex(Vector2 location)
        {
            Location = location;
            Adjacent = new List<Edge>();
            Previous = null;
            Visited = true;
            Distance = 0;
        }

        public void Reset()
        {
            Previous = null;
            Visited = false;
            Distance = float.MaxValue;
        }

        public Vertex GetShortestEdge()
        {
            Edge e = Adjacent.FirstOrDefault(x =>
                x.Destination.Visited != true &&
                x.Cost == Adjacent.Min(y => y.Cost));
            return e != null ? e.Destination : null;
        }

        public void AddEdge(Edge edge)
        {
            if (!Adjacent.Exists(x => x.Destination == edge.Destination))
            {
                Adjacent.Add(edge);
            }
        }

        public override string ToString()
        {
            return Location.ToString();
        }
    }
}
