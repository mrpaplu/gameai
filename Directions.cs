﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace GameAI.Datastructures
{
    public class Directions
    {
        public static List<Vector2> GetDirections(int offset)
        {
            List<Vector2> directions = new List<Vector2>();
            Vector2 steps = new Vector2(offset, offset);

            directions.Add(GetNorth() * steps);
            directions.Add(GetNorthEast() * steps);
            directions.Add(GetEast() * steps);
            directions.Add(GetSouthEast() * steps);
            directions.Add(GetSouth() * steps);
            directions.Add(GetSouthWest() * steps);
            directions.Add(GetWest() * steps);
            directions.Add(GetNorthWest() * steps);

            return directions;
        }

        public static bool IsDiagonal(Vector2 direction)
        {
            direction.Normalize();
            if (direction == GetNorthEast()
                || direction == GetSouthEast()
                || direction == GetSouthWest()
                || direction == GetNorthWest())
                return true;
            return false;
        }

        private static Vector2 GetNorth()
        {
            return new Vector2(0, -1);
        }
        private static Vector2 GetNorthEast()
        {
            return new Vector2(1, -1);
        }
        private static Vector2 GetEast()
        {
            return new Vector2(1, 0);
        }
        private static Vector2 GetSouthEast()
        {
            return new Vector2(1, 1);
        }
        private static Vector2 GetSouth()
        {
            return new Vector2(0, 1);
        }
        private static Vector2 GetSouthWest()
        {
            return new Vector2(-1, 1);
        }
        private static Vector2 GetWest()
        {
            return new Vector2(-1, 0);
        }
        private static Vector2 GetNorthWest()
        {
            return new Vector2(-1, -1);
        }
    }
}
