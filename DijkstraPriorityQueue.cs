﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Datastructures
{
    public class DijkstraPriorityQueue
    {
        Dictionary<Vertex, float> distances = new Dictionary<Vertex, float>();

        public void Add(Vertex vertex, float distance)
        {
            distances.Add(vertex, distance);
        }

        public void Remove(Vertex vertex)
        {
            distances.Remove(vertex);
        }

        public Vertex GetShortestDistance()
        {
            KeyValuePair<Vertex, float> pair = distances.FirstOrDefault(x => x.Value == distances.Min(y => y.Value));
            Remove(pair.Key);
            return pair.Key;
        }

        public bool Any()
        {
            return distances.Any();
        }

        public void UpdateDistance(Vertex vertex, float distance)
        {
            distances[vertex] = distance;
        }
    }
}
