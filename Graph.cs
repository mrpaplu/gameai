﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace GameAI.Datastructures
{
    public class Graph
    {
        public Dictionary<Vector2, Vertex> Vertices { get; private set; }
        public int Offset { get; private set; }

        public Graph(int offset)
        {
            Vertices = new Dictionary<Vector2, Vertex>();
            Offset = offset;
        }

        public void AddEdge(Vector2 source, Vector2 destination, float cost)
        {
            Vertex v = CreateVertex(source);
            Vertex w = CreateVertex(destination);

            v.AddEdge(new Edge(w, cost));
            w.AddEdge(new Edge(v, cost));
        }

        public Vertex CreateVertex(Vector2 location)
        {
            if (Vertices.ContainsKey(location))
            {
                return Vertices[location];
            }
            Vertex v = new Vertex(location);
            Vertices.Add(location, v);
            return v;
        }

        public Vertex GetNearestVertex(Vector2 mousePosition)
        {
            return GetNearestVectorRecursively(mousePosition, Offset);
        }

        public Vertex GetNearestVectorRecursively(Vector2 mousePosition, int offset)
        {
            KeyValuePair<Vector2, Vertex> vertex =
                Vertices.FirstOrDefault(x =>
                    x.Key.X > mousePosition.X - offset
                    && x.Key.X < mousePosition.X + offset
                    && x.Key.Y > mousePosition.Y - offset
                    && x.Key.Y < mousePosition.Y + offset);
            return vertex.Value ?? GetNearestVectorRecursively(mousePosition, offset * 2);
        }

        public Stack<Vertex> GetShortestPath(Vertex source, Vertex destination)
        {
            DijkstraPriorityQueue queue = new DijkstraPriorityQueue();
            Vertex current = null;

            // Initialize all dijkstravertices, set all to visited false and previous null
            // Except for source
            foreach (KeyValuePair<Vector2, Vertex> pair in Vertices)
            {
                Vertex dv = pair.Value;
                if (source != pair.Value)
                    dv.Reset();
                else
                    current = dv;
                queue.Add(dv, dv.Distance);
            }

            if (current == null)
                return null;

            // Calculate every nodes distance 'til destination is found
            while (queue.Any())
            {
                // Get next closest or break if null
                current = queue.GetShortestDistance();
                if (current == null)
                    break;

                foreach (Edge e in current.Adjacent)
                {
                    float distance = current.Distance + e.Cost;
                    if (distance < e.Destination.Distance)
                    {
                        e.Destination.Distance = distance;
                        e.Destination.Previous = current;
                        queue.UpdateDistance(e.Destination, distance);
                    }
                }

                // Return if found
                if (current.Location.Equals((destination.Location)))
                {
                    Stack<Vertex> path = new Stack<Vertex>();
                    while (current.Previous != null)
                    {
                        path.Push(current.Previous);
                        current = current.Previous;
                    }
                    return path;
                }
            }

            return new Stack<Vertex>();
        }
    }
}
