﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Datastructures
{
    public class Edge
    {
        public Vertex Destination { get; set; }
        public float Cost { get; set; }

        public Edge(Vertex destination, float cost)
        {
            Destination = destination;
            Cost = cost;
        }

        public override string ToString()
        {
            return "To " + Destination;
        }
    }
}
